\documentclass[../Mathe_I]{subfiles}
\begin{document}
	

\chapter{Funktionen, Grenzwerte, Stetigkeit}

\section{Reelle Funktionen}
Eine reelle Funktion in einer Variablen ist eine Abbildung
$$f:\ D \to \mathbb R,\ x \mapsto f(x)$$
mit $D \subset \mathbb R$.

\bei 
\begin{description}
	\item[Lineare Funktionen] $f:\ \mathbb R \to \mathbb R,\ f(x) = ax$ (für ein $a \in \mathbb R$)
	\item[Affine Funktionen] $f:\ \mathbb R \to \mathbb R,\ f(x) = ax + b$ (für $a,b \in \mathbb R$)
	\item[Quadratische Funktionen] $f:\ \mathbb R \to \mathbb R,\ f(x) = ax^2 + bx + c$ (für $a,b,c \in \mathbb R$)
	\item[Wurzelfunktionen] $f:\ [ 0,\infty ) \to \mathbb R,\ f(x) = \sqrt{x}$
\end{description}
Die Rechenoperationen der \underline{reellen} Zahlen lassen sich auch für alle Funktionen definieren.

Wir setzen für $f,g:\ D \to \mathbb R$
\begin{align*}
	(f \pm g)(x) &= f(x) \pm g(x)\\
	(f \cdot g)(x) &= f(x) \cdot g(x)\\
	\left(\frac{f}{g}\right)(x) &= \frac{f(x)}{g(x)} \tx{ falls } g(x) \neq 0
\end{align*}

Für $f: D \to \mathbb R,\ g: E \to \mathbb R$ mit $f(D) \subset E$ ist auch die Verkettung definiert.
$$g \circ f: D \to \mathbb R,\ (g \circ f)(x) = g(f(x))$$
Oft interessieren uns Maxima und Minima reeller Funktionen (sogenannte Extremwerte) oder, allgemeiner, Bereiche, in deren die Funktionen wächst bzw. fällt.

Eine Funktion $f: D \to \mathbb R$ heißt monoton wachsend, falls gilt $x_2 > x_1 \Rightarrow f(x_2) \geq f(x_1)$, streng monoton wachsend falls gilt $x_2 > x_1 \Rightarrow f(x_2)> f(x_1)$

\pagebreak
\hfw
\vfill

\wid{Funktionen:
$$f:\ D \to \mathbb R,\ x \mapsto f(x) \qquad D \subset \mathbb R$$
\begin{itemize}
	\item Addition/multiplication: $(f + g)(x) = f(x) + g(x)$
	\item Verkettung: $(f \circ g)(x) = f(g(x))$
	\item Monotonie
\end{itemize}
}

Ein weiterer Begriff beschreibt die Symmetrie von Funtionen:
$f:\ D \to \mathbb R$ heißt \begin{itemize}[]
	\item gerade: $f(-x) = f(x)$ für alle $x \in D$
	\item ungerade: $f(-x) = - f(x)$
\end{itemize}
Achtung: D muss Symmetrisch um den Ursprung sein ($\forall x$ gilt $x \in D \Rightarrow -x \in D$)

\bei $f(x) = x^2 +px-q$ ist genau dann gerade, wenn $p = 0$ ist (und nie ungerade). $f(x) =  \sin x$ ist ungerade.

\section{Polynome und rationale Funktionen}

\subsubsection{Definition 2.1}
Eine Funktion $f:\ \mathbb R \to \mathbb R$ heißt (reelles) Polynom vom \textit{Grad} $n \in \mathbb N_0$. Falls $a_0, a_1, \dots, a_n \in \mathbb R$ existieren mit $a_n \neq 0$ so dass gilt
$$f(x) = a_0 + a_1 x + a_2 X^2 + \dots + a_n x^n \qquad \tx{für alle } x \in \mathbb R$$
Die $a_i,\ i = 0,\dots, n$ heißen \textit{Koeffizienten} des Polynoms, $a_n$ heißt \textit{Leitkoeffizient}.

\subsubsection{Bemerkung:} 
\begin{itemize}[-]
	\item Ein Polynom von Grad Null ist eine konstante Funktion
	\item Das \textit{Nullpolynom} (= \textit{Nullfunktion}) wird separat behandelt (Ihr wird üblicherweise den Grad -1 zugeordnet)
\end{itemize}
\subsubsection{Lemma 2.2 Abspalten von Linearfunktionen}
Hat ein Polynom $f(x)$ vom Grad $n \in \mathbb N$ eine Nullstelle $\lambda \in \mathbb R$ (also $f(\lambda) = 0$) so gibt es ein Polynom $g(x)$ (nicht das Nullpolynom) von Grad $n-1$ mit 
$$f(x) = (x - \lambda) \cdot g(x) \qquad \tx{für alle } x \in \mathbb R$$

\textit{Beweis:} Sei $f(x) = a_0 + a_1 x + \dots + a_n x^n$ mit $a_n \neq 0$, und sei $f(\lambda) = 0$. Im Fall $\lambda = 0$ folgt $a_0 = 0$ und somit 
$$f(x) = x(a_1 + a_2 x + \dots + a_n x^{n-1})$$
Sei nun $f(x) = 0$ für $\lambda \in \mathbb R$ Wir betrachten
$$\tilde f(x) = f(x + \lambda) = a_0 + a_1 (x + \lambda) + \dots + a_n (x + \lambda)^n$$
Wir sehen (durch Ausmultiplizieren und Sortieren), dass $\tilde f(x)$ ein Polynom vom Grad $n$ mit Leitkoeffizient $a_n$ ist. Es gilt $\tilde f (0) = f(\lambda) = 0$ Somit existiert ein Polynom $\tilde g$ vom Grad $n - 1$ mit $\tilde f(x) = x \cdot \tilde g(x)$ bzw. $f(x) = \tilde f(x - \lambda) = (x - \lambda) \cdot \tilde g(x - \lambda)$ und $\tilde g(x - \lambda)$ ist (analog zu vorher) ein Polynom von Grad $n - 1$ und wir setzen $g(x) = \tilde g(x - \lambda)$ \hfill $\Box$

\subsubsection{Lemma 2.3 Zahl der Nullstellen von Polynomen}
Sei $f:\ \mathbb R \to \mathbb R$ ein Polynom vom Grad $n \in \mathbb N_0$. Dann hat $f$ mindestens $n$ verschiedene Nullstellen.

\textit{Beweis:} (durch Induktion) Für $n = 0$ ist $f(x) = a_0$ für alle $x \in \mathbb R$, wobei $a_0 \neq 0$ (denn $f$ ist \underline{nicht} das Nullpolynom). Diese Funktion besitzt keine Nullstelle.


Sei $f$ ein Polynom vom Grad $n \in \mathbb N$ 
\begin{itemize}
	\item Falls $f$ keine Nullstelle besitzt, so ist der Beweis zu Ende.
	\item Falls $f$ eine Nullstelle $\lambda \in \mathbb R$ besitzt, so gibt (Nach Lemma 2.2), dass
	$$f(x) = (x - \lambda) g(x)$$ 
	mit $g$ vom Grad $n - 1$. Nach Induktionsannahme hat $g$ höchstens $n - 1$ Nullstellen. Damit hat $f$ höchstens $n$ Nullstellen. \hfill $\Box$
\end{itemize}

\subsubsection{Lemma 2.4 Koeffizientenvergleich}
Seien $f,g:\ \mathbb R \to \mathbb R$ Polynome vom Grad $m$ bzw. $n$, dass heißt es gibt mit $a_m \neq 0$, $b_n \neq 0$, dass
$$f(x) = \summ{i=0}{m}a_i x^i,\quad g(x) = \summ{i = 0}{n} b_i x^i$$
für alle $x \in \mathbb R$

Sind $f(x)$ und $g(x)$ an mehr als $\max(m,n)$ verschiedenen Stellen gleich, so gilt $m = n$ und $a_i = b_i$ für alle $i = 1, \dots, n$.

\textit{Beweis:} Angenommen $ m \neq n$ oder (falls $m = n$) sei $b_i \neq a_i$ für ein $i = 1, \dots, n$.
Dann ist $f(x) - g(x)$ ein Polynom von Grade nicht höher als $\max(m,n)$. Damit hat $f(x) - g(x)$ aber nach Lemma 2.4 höchstens $\max(m,n)$ Nullstellen \hfill $\Box$

Die Funktion $f(x) = x^2 + 1$ hat aber z.B. keine (reellen) Nullstellen. Um Polynome wirklich zu verstehen, müssen wir die komplexen Zahlen betrachten. Ein komplexes Polynom vom Grad $n \in \mathbb N_0$ hat die Form:
$$f:\ \mathbb C \to \mathbb C,\ f(z) = a_0 + a_1 z + a_2 z^2 + \dots + a_n z^n \quad (\tx{mit } a_n \neq 0)$$
Das Abspalten von Linearfunktionen ändert sich nicht (wir haben nur Rechenregeln verwendet, die auch im Komplexen funktionieren). Ins besondere besitzt auch ein komplexes Polynom vom Grad $n$ höchstens $n$ verschiedene Nullstellen.

Im Unterschied zum Reellen gilt nun aber der
\subsubsection{Satz 2.5 Fundamentalsatz der Algebra}
Jedes komplexe Polynom vom Grad $n \geq 1$ besitzt mindestens eine Nullstelle $\lambda \in \mathbb C$

\wid{Polynome (\& Rationale Funktionen), Anzahl Nullstellen
$$f(x) = \summ{i=0}{n}a_j x^j = a_0 + a_1 x + a_2 x^2 + \dots + a_n x^n$$
$$a_j \in \mathbb C,\ f:\ \mathbb C \to \mathbb C \rightarrow \tx{ komplex Polynom}$$
oder
$$a_j \in \mathbb R,\ f:\ \mathbb R \to \mathbb R \rightarrow \tx{ reelles Polynom}$$
$\lambda$ Nullstellen $\rightarrow\ f(x) = (x-\lambda) \cdot g(x)$

Jedes komplexe Polynom con Grad $n \geq 1$ besitzt mindestens eine komeplexe Nullstelle $\lambda \in \mathbb C$
}

\subsubsection{Folgerung 2.6 Polynomfaktorisierung}
Ein Polynom $f:\ \mathbb C \to \mathbb C$ vom Grad $n \geq 1$ hat eine Zerlegung
$$f(z) = a_1 \prod_{k=1}^{K} (z - \lambda_k)^{n_k}$$
für alle $z \in \mathbb C$

Dabei sind $\lambda_1, \dots, \lambda_k \in \mathbb C$ die Nullstellen mit Zugehörigen Vielfachen $n_1, \dots, n_k \in \mathbb N$ und es gilt $n = n_1 + n_2 +\dots + n_k$, und $a_k \in \mathbb C\setminus \{ 0 \}$ ist der Leitkoeffizient von $f$.

\textit{Beweis:} Es folgt mit Satz 2.5, das $\lambda$ eine Nullstelle $\lambda_1 \in \mathbb C$ besitzt nach abspalten folgt $f = (z - \lambda_1) f_1 (z)$ mit $f_1 (z)$ komplexe Polynom vom Grad $n - 1$.

Falls $n -1 \geq 1$ so besitzt $f_1 (z)$ wieder eine Nullstelle. Ist diese gleich $\lambda_1$, so folgt 
$$f(z) = (z - \lambda_1)^2 f_2 (z)$$
sonst
$$f(z) = (z - \lambda_1) (z - \underbrace{\lambda_2}_{\mathclap{\tx{2. Nullstelle}}}) f(z)$$
Der Prozess stoppt nach $n$ Schritte, der $f_n(z)$ ist vom Grad Null und $f_n (z) = a_n$
\hfill $\Box$

$$a_n:\ (z - \lambda_1)^{n_1} \cdot (z - \lambda_2)^{n_2} \cdot \dots \cdot (z - \lambda_k)^{n_k}$$
Zurück zum reellen Fall.

Wir fassen ein reelles Polynom als komplexes Polynom auf (durch einsetzten komplexer Zahlen für $x$.) Wir bekommen dann:

\subsubsection{Folgerung 2.7 Polynom faktoriesierung in $\mathbb R$}
Jedes reelle Polynom $f:\ \mathbb R \to \mathbb R$ vom Grad $n \geq 1$ zerfällt in lineare und quadratische Faktoren. Genauer 
$$f(x) = a_n \prod_{j = 1}^{J} (x - \lambda)^{l_j} \cdot \prod_{k = 1}^{K} (x^2 - 2 \alpha_k + \alpha^2_k + \beta_k^2)^{m_k}$$
Dabei sind $\lambda_j \in \mathbb R$ die reellen Nullstellen von $f(x)$ und $\alpha_k \pm i\beta_k$ sind die komplexen Nullstellen von $f(x)$.

Für die Vielfachheiten gilt 
$$n = l_1, \dots l_J + 2(m_1 + \dots + m_K)$$
(Die $\beta_k$ sind nicht gleich Null)

\textit{Beweis:} Wir können annehmen, dass $f(x)$ keine reellen Nullstellen besitzt (sonst spalten wir diese einfach ab). Weiter gilt Falls $\mu = \alpha + i \beta$ eine (nicht reelle) Nullstelle ist so ist auch $\bar \mu = \alpha - i \beta$ eine Nullstelle. Dann 
\begin{align*}
0 = \overline 0 = \overline{f(\mu)} &= \overline{a_0 + a_1 \mu + \dots + a_n \mu^n}\\
&= \overline{a_0} + \overline{a_1 \mu} + \dots + \overline{a_n \mu^n}\\
&= \overline{a_0} + \overline{a_1} \cdot \overline{\mu} + \dots + \overline{a_n} \cdot \overline{\mu^n}\\
&= a_0 + a_1 \cdot \overline{\mu} + \dots + a_n \cdot \overline{\mu^n} = f(\overline{\mu})
\end{align*}

Nun kommen wir jeweils nacheinander $z - \mu$ und $z - \overline{\mu}$ als Faktoren abspalten. Wir setzen $\mu = \alpha + i \beta$ und erhalten 
$$f(z) = (z - \mu)(z - \overline{\mu}) g(z) = z^2 - 2 \alpha z + \alpha^2 + \beta^2)g(z)$$
Damit erhalten wir die gemischte Zerlegung.

Um fortzufahren müssen wir Zeigen, dass $g(z)$ wieder nur reelle Koeffizienten besitzt. Wir setzen
$$g(z) = b_>0 + b_1 z + \dots + b_{n-2} z^{n-2}$$
mit (zumindest $b_j \in \mathbb C$) 

Wir setzen $x \in \mathbb R$ in $f$ ein und erhalten

\begin{align*}
0 &= \Im (f(x)) \tx{ (denn } f(x) \tx{ hat reelle koeffizienten)}\\
&= (x^2 - 2\alpha x + \alpha^2 + \beta^2) \Im (g(x))\\
&= (\dots) \summ{j = 0}{n - 2} (\Im (b_j)) x^j
\end{align*}

Die klammer links ist für alle $x \in \mathbb R$ \underline{ungleich} Null. Also muss gelten 
$$\summ{j = 0}{n-2} (\Im(b_j)) x^j = 0 \quad \tx{für alle } x \in \mathbb R$$
Mit Lemma 2.3 folgt, dass $\Im(b_j) = 0$ für alle $j = 0, \dots, n -2$ (denn sonst hätten wir ein Polynom vom Grad $n \geq 0$ mit mehr als $n$ Nullstellen. \hfill $\Box$

\subsubsection{Bemerkung}
Die Existenz von Nullstellen liefert Satz 2.5. Das Berechnen von Nullstellen ist eine andere Frage.

% 2.12.19

\wid{Polynome: Faktorisierung in $\mathbb C$ 
$$f(z) = a_n \prod_{k=1}^K (z - \lambda_k)^{n_k}$$
in $\mathbb R$
$$f(x) = a_n \prod_{j = 1}^J (x - \lambda)^{c_j} \cdot \prod_{k = 1}^K (x^2 - 2x \dots$$ 
}

Eine rationale Funktion ist ein Quotient zweier Polynome. Seien $p(x), q(x)$ Polynome Grad $m$ bzw. $n$, und sei $N_q$ die (endliche) Menge der Nullstellen von $q$. Dann ist $f:\ \mathbb R\setminus N_q$, $f(x) = \frac{p(x)}{q(x)}$ eine rationale Funktion. Wie Zerlegen rationale Funktionen wie folgt.

\subsubsection{Lemma 2.8}
Seien $p(x) = a_m x^m + \dots + a_0$ und $q(x) = b_n + \dots + b$. Polynome mit $m \geq n$, und $f(x) = \frac{p(x)}{q(x)}$. Dann gibt es eindeutig bestimmte Polynome $g(x)$ und $p(x)$ mit $r(x)$ vom Grad $k < n$ (eventuell das Nullpolynom) mit $f(x) = g(x) + \frac{r(x)}{q(x)}$ für alle $x \in \mathbb R \setminus N_q$

\textit{Beweis:} Wir machen Division mit Rest für Polynome, wir Schrieben:
\begin{equation}\label{f}
	\frac{p(x)}{q(x)} = \frac{a_m}{b_n}x^{m-n} + \frac{p_1 (x)}{q(x)} \tag{$\star$}
\end{equation}
mit
$$p_1(x) = p(x) - \frac{a_m}{b_n} x^{m-n} q(x)$$
wir sehen, dass gilt $\grad p_1 < m$. Damit ist der Fall $m = n$ erledigt (mit $g(x) = \frac{a_m}{b_n}$ und $r(x) = p_1(x)$).

Die Zerlegung in ($\star$) % \eqref{f} 
ist machbar, solange der Grad des Zählers auf der einen Seite nicht kleiner als $n = \grad q$ ist. Damit können wir falls $\grad p_1 \geq n$ weiter Zerlegen als 
$$\frac{p_1}{q(x)} = \frac{\custoup{\rightarrow}{c}{\mathclap{\tx{ Leitkoeffizient von } p_1}}}{b_n}x^{\grad p_1 - m} + \frac{p_2(x)}{q(x)}$$
u.s.w. bis $\grad p_j < n$ (und wir setzen $r(x) = p_j(x)$.

Zur Eindeutigheit: Angenommen es gilt $$\frac{p(x)}{q(x)} = g_1(x) + \frac{p_1(x)}{q(x)} = g_2(x) + \frac{r_2(x)}{q(x)}$$, für alle $x \in \mathbb R$ Dann folgt 
\begin{equation}\label{2}
	(g_1 - g_2) \cdot q(x) + r_1(x) - r_2(x) = 0 \tag{$\star \star$}
\end{equation}
für $x \in \mathbb R \setminus N_q$. Falls $g_1 + g_2$, dann ist $(g_1(x) - g_2(x))q(x)$ ein Polynom vom Grad größer oder gleich $n$. $r_1(x),r_2(x)$ sind vom Grad \underline{kleiner} als $n$. Somit ist die linke Seite von ($\star \star$) % \eqref{2}
 ein Polynom vom Grad größer oder gleich $n$. Das kann nicht sein wegen Lemma 2.3. Ebenso für $r_1(x),r_2(x)$ soeben $g_1(x),g_2(x)$. \hfill $\Box$
 
 \bei 
 $$p(x) = x^4 - x^3 + x^2 - x + 1, \quad q(x) = x^2 + 2x$$
 \begin{align*}
 	(x^4 - x^3 + x^2 - x + 1):(x^2 + 2x) &= x^2 + \frac{-3x^3 + x^2 - x + 1}{x^2 + 2x}\\
 	(-3x^3 + x^2 - x + 1):(x^2 + 2x) &= 3x + \frac{7x^2 - x + 1}{x^2 + 2x}\\
 	(7x^2 - x + 1):(x^2 + 2x) &= 7 + \frac{-15x + 1}{x^2 + 2x}
 \end{align*}
Also gilt:
$$\frac{x^4 - x^3 + x^2 - x + 1}{x^2 + 2x} = x^2 - 3x + 7 + \frac{-15x + 1}{x^2 + 2x}$$

Die Nullstellenmenge von $q$, $N_q$ ist auch von Interesse. Sei $\lambda$ eine $l$-fache Nullstelle von $q(x)$ (Dass heißt der Linearfaktor $(x - \lambda)$ lässt sich $l$-mal abspalten.) und eine $k$-fache Nullstelle von $p(x)$ ($k = 0$ Falls $\lambda$ keine Nullstelle von $p(x)$ ist.) Damit ergeben sich Polynome $p_1(x), q_1(x)$ mit $p_1(\lambda) \neq 0, q_1(\lambda) \neq 0$, so dass
$$f(x) = \frac{p(x)}{q(x)} = \frac{(x - \lambda)^k}{(x - \lambda)^l} \frac{p_1(x)}{q_1(x)} = (x- \lambda)^{k-l}\frac{p_1(x)}{q_1(x)}$$
Das verhalten von $f(x)$ nahe $\lambda$ hängt von Exponenten $k-l$ ab.
\begin{enumerate}[i)]
	\item für $k \geq l$ kann $f(x)$ auch in punkt $\lambda$ sinnvoll definiert werden, nämlich 
		$$f(x) = \casess{\frac{p_1(\lambda)}{q_1(\lambda)}}{\tx{falls }k = l}{0}{\tx{falls }k > l}$$
\end{enumerate}

\hfw

% Vorlesung aufschrieb

% 4.12.19

\wid{Bisektionsverfahren, Polynome, Nullpolynom}

\section{Kreisfunktionen}

Die trigonometrischen Funktionen haben wir bereits mehrfach gesehen. Sei $D_\varphi:\ \mathbb R^2 \to \mathbb R^2$ die Drehung um den Winkel $\varphi \in \mathbb R$ (im Bogenmaß). Für $\varphi > 0$ sehen wir im mathematisch positiven Sinn, d.h. gegen den Uhrzeigersinn. \lcom{Das ist einfach Konvention.} (im Uhrzeigersinn für $\varphi < 0$) Wir setzen $D_\varphi e_1 = \begin{pmatrix}
	\cos \varphi\\
	\sin \varphi
\end{pmatrix},\ D_\varphi e_2 = \begin{pmatrix}
	-\sin \varphi \\ 
	\cos \varphi
\end{pmatrix}$ wobei $e_1 = \begin{pmatrix}
	1\\ 0
\end{pmatrix},\ e_2 = \begin{pmatrix}
	0\\ 1
\end{pmatrix}$. Dies definiert uns die Funktionen sin, cos: $\mathbb R \to \mathbb R$. Wir bekommen sofort 
$$\sin^2 \varphi + \cos^2 \varphi = 1 \qquad (\sin^2 \varphi = (\sin \varphi)^2)$$
Es folgt auch 
$$-1 \leq \cos \varphi,\ \sin \varphi \leq 1$$
und
$$\underbrace{\cos(-x) = \cos(x)}_{\tx{gerade}} \quad \underbrace{\sin(-x) = - \sin x}_{\tx{ungerade}}$$
Die Funktionen sind $2\pi$-periodisch, d.h. $\sin(\varphi + 2k\pi ) = \sin \varphi$, $\cos(\varphi + 2k\pi ) = \cos \varphi$ für $k \in \mathbb Z$
\begin{align*}
	\sin \varphi &= 0 \Leftrightarrow \varphi \in \mathbb Z \pi \\
	\cos \varphi &= 0 \Leftrightarrow \varphi \in (\equalto{2 \mathbb Z + 1) \frac{\pi}{2}}{\mathclap{\{\Psi \in \mathbb R:\ k \in \mathbb Z,\ \exists \Psi = (2k + 1)\frac{\pi}{2}\}}} 
\end{align*}
Zum Zeigen der Additionstheoreme brauchen wir eine besondere Eigenschaft der Abbildung $D_\varphi$, welch $D_\varphi (v + w) = D_\varphi v + D_\varphi w$ und $D_\varphi (\lambda v) = \lambda D_\varphi$ für alle $v,w \in \mathbb R^2, \lambda,\varphi \in \mathbb R$. (Solche Funktionen heißen lineare Funktionen.)

%\begin{tikzpicture}
%	\draw[->] (-1,0) -- (3,0);
%	\draw[->] (0,-1) -- (0,2);
%\end{tikzpicture}

\subsubsection{Satz 3.1 Additionstheoreme}
Für alle $x,y \in \mathbb R$ gilt 
\begin{align*}
	\cos(x+y) &= (\cos x) \cdot (\cos y) - (\sin x)(\sin y)\\
	\sin(x+y) &= (\sin x) \cdot (\cos y) + (\cos x)(\sin y)
\end{align*}

\textit{Beweis:} Es gilt $D_{x+y} = D_{y} \circ D_x$, und somit 
$$\begin{pmatrix}
	\cos(x+y)\\ \sin (x+y)
\end{pmatrix} = D_{x+y} e_1 = (D_y \circ D_x) e_1 = D_y (D_x e_1) = D_y \left( \begin{pmatrix}
	\cos x\\ \sin x
\end{pmatrix} \right) = D_y ((\cos x) e_1 + (\sin x) e_2) =$$

(
$$\cos x \cdot \begin{pmatrix}
	1\\ 0
\end{pmatrix} = \begin{pmatrix}
	\cos x\\ 0
\end{pmatrix} \quad \sin x \cdot \begin{pmatrix}
	0\\ 1
\end{pmatrix} = \begin{pmatrix}
	0\\ \sin x
\end{pmatrix}$$
)

$$= \cos x \cdot D_y e_1 + \sin x D_y e_2 = \cos x \begin{pmatrix}
	\cos y \\ \sin y
\end{pmatrix} + \sin x \begin{pmatrix}
	-\sin y\\ \cos y
\end{pmatrix} = \begin{pmatrix}
	\cos x \cos y - \sin x \sin y\\ \cos x \sin y + \sin x \cos y
\end{pmatrix}$$
\hfill $\Box$

Spezialfall $y = \pm \frac{\pi}{2}$
$$\sin(x+ \frac{\pi}{2}) = \cos x,\quad \cos (x - \frac{\pi}{2}) = \sin x$$

\subsubsection{Definition 3.2}
 Die Tangens- bzw. Cotangensfunktion ist definiert durch 
 \begin{align*}
 	\tan x &= \frac{\sin x}{\cos x} \quad \tx{für } x \neq (2 \mathbb Z + 1) \frac{\pi}{2}\\
 	\cot x &= \frac{\cos x}{\sin x} \quad \tx{ für } x \neq \mathbb Z \pi
 \end{align*}

Wir bezeichnen 
$$\left. \begin{array}{c}
	\tan(-x) = - \tan x\\
	\cot(-x) = -\cot x
\end{array} \right \} \tx{ ungerade}$$

$$\left. \begin{array}{c}
	\tan(x+\pi) = \tan x\\
	\cot(x+\pi) = \cot x
\end{array} \right \} \tx{ Periode }\pi$$

Zurück zur Darstellung von $\mathbb C$ als $\mathbb R^2$ und der Polardarstellung komplexer Zahlen.

Es gilt $z = r(\cos \varphi + i \sin \varphi)$ mit $r>0, \varphi \in \mathbb R$ für $z \in \mathbb C\setminus \{0\}$.
Wir schreiben
$$e^{ix} = \cos x + i \sin x$$
für $x \in \mathbb R$ (Euler'sche Formel)

Warum? Die (aus der Schule) bekannte Exponentialfunktion erfüllt
$$e^{\lambda(x+y)} = e^{\lambda x + \lambda y} = e^{\lambda x} \cdot e^{\lambda y}$$
für alle $x,y,\lambda \in \mathbb R$. (Wird später noch bewiesen)

Das selbe Gesetz angewendet auf $\lambda = i \in \mathbb C$
\hfw

% 09.12.19

\lcom{Neue Klausur information in Kapitel 0 (in rot)}

\lcom{Um die Umkehrfunktion von cos zu bestimmen müssen wir uns Sinnvoller weise einschränken da cos offensichtlich nicht injektiv ist (da es Periodisch ist)}
$$\cos: [0,\pi] \to [-1,1]$$
Diese Funktion ist streng monoton fallend und damit injektiv. Es gilt $0\leq x < y \leq \pi \Rightarrow \cos x > \cos y$. Um das zu beweisen, rechnen wir
$$e^{iy} - e^{ix} = e^{i\frac{x + y}{2}} (-e^{i\frac{x - y}{2}} + e^{i\frac{y-x}{2}}) = 2ie^{i\frac{x + y}{2}} \cdot \sin \frac{y - x}{2}$$
Wir bilden die Realteile und bekommen
$$\cos y - \cos x = -2 \underbrace{\overbrace{\sin \underbrace{\frac{x + y}{2}}_{\in (0,\pi)}}^{>0} \overbrace{\sin \underbrace{\frac{y - x}{2}}_{\in(0,pi)}}^{>0}}_{<0}$$
Es gilt $\sin(t) > 0$ für $t \in (0,\pi)$.

Wir definieren die Umkehrfunktion 
$$\arccos:[-1,1] \to [0,\pi]; \quad \cos(\arccos(t)) = t$$
Damit gilt auch $\arccos (\cos x) = x$ für $x \in (0,\pi]$. Wir können nun die Polarkoordinaten von $z = x + iy + 0$ angeben.
mit
$$r = \sqrt{x^2 + y^2}, \quad \varphi = \casess{\arccos \frac{x}{r}}{\tx{für } y \geq 0}{2 \pi - \arccos \frac{x}{r}}{\tx{für }y < 0}$$

%\begin{figure*}
%\centering
%\begin{tikzpicture}
%	\draw[->,thick] (-1.5,0) -- (1.5,0);
%	\draw[->,thick] (0,-1.5) -- (0,1.5);
%	\draw (1,0) arc (0 : 270 : 1);
%\end{tikzpicture}
%\end{figure*}

\underline{Wichtige} Anwendungen der trigonometrischen Funktionen. Beschreibung von Schwingungen.

Wir sagen $f:\ \mathbb R \to \mathbb R$ heißt periodisch mit Periode $T > 0$, falls gilt
$$f(t + T) = f(t),\quad \tx{für alle } t \in \mathbb R$$
Falls $f$ periodisch mit Periode $T$, so folgt sofort, dass $f$ periodisch mit Periode $kT$ für alle $k \in \mathbb Z\setminus \{0\}$.
Mit ,,der Periode`` eine periodische Funktion bezeichnen wir das kleinste $T > 0$, so dass $f$ $T$-periodisch ist.

Eine \textit{harmonische Schwingung} abhängig \underline{von} $t > 0$ (zeit) ist eine Funktion der Form
$$x:\ \mathbb R \to \mathbb R; \quad x(t) = A \cdot \cos (\omega t + \alpha)$$
\begin{itemize}
	\item Schwingungsdauer (= Periode der Schwingung) ist $\frac{2\pi}{\omega} = T$
	\item Frequenz $v = \frac{1}{T}$
	\item Amplitude $A > 0$
	\item Phasenwinkel $\alpha \in \mathbb R$
	\item Kreisfrequenz $\omega > 0$
\end{itemize}

Die Überlagerung (Addition) zweier Schwingunen $x_1(t), x_2(t)$ ist gegenen durch $x(t) = x_1(t) + x_2(t)$. Im allgemeinen ist $x(t)$ \underline{nicht} periodisch. 

Ausnahme: $\frac{T_2}{T_1} \in \mathbb Q$, also $\frac{T_2}{T_1} = \frac{n_1}{n_2}$, mit $n_1,n_2 \in \mathbb N$
Dann besitzt $x(t)$ die Periode $n_1 T_1 = n_2 T_2$, was eine Periode sowohl von $x_1(t)$ also auch von $x_2(t)$ ist.

\subsubsection{Satz 3.4}
Seien $x_{1,2}(t)$ harmonische Schwingungen mir der selben Kreisfrequenz, also 
$$x_1 (t) = A_1 \cos (\omega t + \alpha_1), \quad x_2 (t) = A_2 \cos (\omega t + \alpha_2)$$
Dann ist $x(t) = x_1 (t) + x_2(t)$ wieder eine harmonische Schwingung und es gilt
$$x(t) = A \cos(\omega t + \alpha) \tx{ mit } Ae^{i\alpha} = A_1 e^{i\alpha_1} + A_2 e^{i\alpha_2}$$

\textit{Beweis:} Es ist hier praktisch, im Komplexen zu rechnen. Wir schreiben
$$z_1 (t) = A_1 e^{i(\omega t + \alpha_1)}, \quad z_2 (t) = A_2 e^{i(\omega t + \alpha_2)}$$
Wir rechnen
$$z_1(t) + z_2(t) = e^{i\omega t}(A_1 e^{i\alpha_1} + A_2 e^{i\alpha_2}) = A \cdot e^{i\alpha} \cdot e^{i\omega t} = Ae^{i(\omega t + \alpha)}$$

\subsubsection{Bemerkung:}
Für $A_1 = A_2$, $\alpha_1 = 2(k + \frac{1}{2} \pi + \alpha_2$, $k \in \mathbb Z$ gilt $A = 0$, was nach unserer Definition keine harmonische Schwingung ist. Dann ist $x(t) = 0$ für alle $t \in \mathbb R$.

\section{Zahlenfolgen und Grenzwerte}
Zurück zu unserem Beispiel: Es sei
$$f(x) = a_0 + a_1 x + a_2 x^2 + a_3 x^3 + a_4 x^4 + a_5 x^5$$
Wir suchen eine Nullstelle. Es gelte
$f(v) \cdot f(w) < 0$ $v < w$ für $v,w \in \mathbb R$

\begin{enumerate}
	\item $x_0 = \frac{v + w}{2}$ 
	% bild 2 - 3
		Dann setzen wir $$\left\{ \begin{array}{c}
			v_1 = v\\
			w_1 = x_0\\
			\\
			v_1 = x_0\\
			w_1 = w
		\end{array} \right. \begin{array}{c}
			\tx{falls } f(v) \cdot f(x_0) < 0\\
			\\
			\\
			\tx{sonst.}
		\end{array}$$
	\item Wir setzen $x_1 = \frac{v_1 + w_1}{2}$ und 
		$$\left\{ \begin{array}{c}
			v_2 = v_2\\
			w_2 = x_1\\
			\\
			v_2 = x_1\\
			w_2 = w_1
		\end{array} \right. \begin{array}{c}
			\tx{falls } f(v_1) \cdot f(x_0) < 0\\
			\\
			\\
			\tx{sonst.}
		\end{array}$$
\end{enumerate}
Wir bekommen Zahlen $x_k \in \mathbb R$ für alle $k \in \mathbb N_0$ Behauptung: Es gibt eine Nullstelle $x^* \in [v,w]$, also $f(x^*) = 0$ und $x_k$ kommt für große $k \in \mathbb N$ dieser Nullstelle ,,immer näher``. Frage: was heißt hier ,,immer näher``.

%\begin{lstlisting}
%from math import sin
%def f(x):
%	return sin(x)
%
%
%a = -1.0
%b = 1.01
%eps = 1e-14
%
%print f(a)
%print f(b)
%print 
%
%while b - a > eps:
%	x = (b + a) / 2.0
%	print x
%	if f(a) * f(x) =< 0:
%		b = 0
%\end{lstlisting}

% 11.12.19
Eine Folge reeller Zahlen $a_1, a_2, a_3, \dots$ ist streng genommen eine Abbildung von $\mathbb N$ nach $\mathbb R$. Um eine Folge anzugeben, kann man entweder ein paar Folgenglieder explizit angeben, ein Bildungsgesetz angeben oder eine Rekursionsborschrift angeben. 

\bei Folge der Quadratzahlen: 
\begin{itemize}
	\item $a_n = 1, 4, 9, 16, \dots$
	\item $a_n = n^2$ für $n \in \mathbb N$
	\item $a_{n+1} = (\sqrt{a_n} + 1)^2,\ a_1 = 1$
\end{itemize}

Manchmal ist es sinnvoll, die Nummerierung bei Null zu beginnen. 

\bei \begin{enumerate}[i)]
	\item $a_n = a$ konstante Folge $a,a,a,a,\dots$
	\item $a_n = n$ Folge der natürlichen Zahlen $1,2,3,\dots$
	\item $a_n = a_0 + nd,\ n = 0,1,2,\dots$ arithmetische Folge $a_0,\ a_0 + d,\ a_0 + 2d,\ a_0 + 3d, \dots$
	\item $a_n = a_0 - q^n,\ n = 0,1,2,\dots$ geometrische Folge $a_0,\ a_0 q,\ a_0 q^2,\ a_0 q^3, \dots$
	\item $a_n = n \cdot a_{n-1},\ a_0 = 1$ $1,1,2,6,24,\dots,$ bzw. $a_n = n!$
\end{enumerate}

\subsubsection{Definition 4.1 Konvergenz}
\lcom{Vielleicht die wichtigste definition der ganzen Vorlesung.}

Die Folge $a_n$ \textit{konvergiert} gegen $a \in \mathbb R$, falls gilt zu jedem $\varepsilon > 0$ gibt es ein $N \in \mathbb N$ so dass für alle $n < N$ gilt $|a_n - a| < \varepsilon$. Die Zahl $a$ heißt \textit{Grenzwert} der Folge. Wir schreiben dann 
$$\lim_{n \to \infty} a_n = a$$

\lcom{lim (limes) ist einfach lateinisch für Grenzwert}

oder 
$$a_n \to a \tx{ für } n \to \infty$$
Die Folge $a_n$ heißt \textit{konvergent}, wenn sie gegen ein $a \in \mathbb R$ konvergiert. \textit{Divergent} bedeutet nicht konvergent.

%\begin{tikzpicture}
%	\draw[thick] (-2,0) -- (2,0);
%	\draw (-1,0.1) -- (-1,-0.1) node[yshift=.2] {$a$}; 
%	\node[below] (-1,0) {$-1$};
%\end{tikzpicture}

Die Zahl $\varepsilon > 0$ ist eine Art Fehlerschranke, und gibt vor wie groß der Abstand zwischen $a_n$ und $a$ sein darf. Üblicherweise sind die ersten Folgenglieder zu weit entfernt, für Konvergenz muss es aber ein $N$ geben, so dass der Abstand für alle Folgenglieder die verlangte Grenzweite nicht überschreitet. Typischerweise muss $N$ für kelineres $\varepsilon$ größeres gewählt werden.

In Quantorenschreibweise:
$$\forall \varepsilon > 0 \exists N \in \mathbb N:(n > N \Rightarrow |a_n - a| < \varepsilon)$$

\bei Harmonische Folge: Die Folge $a_n = \frac{1}{n}$ konvergiert gegen $a = 0$. 

\begin{figure*}[h]
\centering
\begin{tikzpicture}
	\draw[thick,<->] (-1,0) -- (2,0);
	\draw[thick] (1.5,0.1) -- (1.5,-0.1);
	\draw[thick] (0,0.1) -- (0,-0.1);
	\draw (0.75,-0.1) -- (0.75,0);
	\draw (0.5,0.1) -- (0.5,0);
	\draw (0.35,-0.1) -- (0.35,0);
	\node[below] at (1.5,0) {$1$};
	\node[below] at (0,0) {$0$};
	\node[above] at (1.5,0) {$a_1$};
	\node[below] at (0.75,0) {$a_2$};
	\node[above] at (0.5,0) {$a_3$};
	\node[below] at (0.3,0) {$a_4$};
	\draw[thick,red,|<-] (0,0.5) -- (1.5,0.5);
\end{tikzpicture}
\end{figure*}

denn zu gegebenen $\varepsilon > 0$ wählen wir 
$$N = \underbrace{\left\lceil\frac{1}{\varepsilon}\right\rceil}_{\mathclap{\frac{1}{\varepsilon}\tx{ aufgerundet auf die nächste natürliche Zahl}}}$$
Wir sehen, dass für diese Wahl von $N$ und alle $n > N$ gilt
$$|a_n - a| = |\frac{1}{n} - 0| = \frac{1}{n} < \frac{1}{N} \leq \varepsilon$$

\bei Konstante Folge: ist $a_n = a$ für alle $n \in \mathbb N$, so folgt:
$$\lim_{n \to \infty} a_1 = a$$
denn 
$$|a_n - a| = |a - a| = 0 < \varepsilon$$
 für alle $\varepsilon > 0$. Wir können also $N = 1$ wählen, unabhängig von $\varepsilon$.

$$a_n \casess{\frac{1}{n}}{\tx{für } n \tx{ gerade}}{\frac{10}{n}}{\tx{für } n \tx{ ungerade}}$$
$$\Rightarrow a_n \to 0 \tx{ für } n \to \infty$$
Für $\varepsilon > 0$ wählen wir $N = \left\lceil\frac{10}{\varepsilon}\right\rceil$, dann folgt 
$$|a_n - a| = |a_n| \leq \frac{10}{n} \underbrace{<}_{\mathclap{\tx{für alle }n>N}} \frac{10}{N} \leq 10\cdot \frac{\varepsilon}{10} \leq \varepsilon$$

\bei Geometrische Folge: Sei $q \in \mathbb R$ Mit $|q| < 1$. Dann gilt 
$$\lim_{n \to \infty} q^n = 0$$
Um das zu Zeigen, können wir $q \neq 0$ annehmen, denn für den Fall $q = 0$ ist die Sache trivial.
Wir haben dann $\frac{1}{|q|}>1$, dammit gilt $\frac{1}{|q|} = 1 + x$ für ein $x > 0$.
Damit können wir rechnen
$$|q^n - 0| = |q^n| = |q|^n = \left(\frac{1}{1 + x}\right)^n = \frac{1}{(1 + x)^n} \leq \frac{1}{1 + nx} \leq \frac{1}{nx} < \varepsilon$$
für alle $n > \frac{1}{\varepsilon x}$ zu $\varepsilon > 0$ können wir also für die Definition der Konvergenz $N = \left\lceil\frac{1}{\varepsilon x}\right\rceil$ verwenden.

\emph{Bemerkung:} Wir haben bewiesen, dass gilt 
$$(1 + x)^n \geq 1 + nx \tx{ für } x > 0$$
(Bernoulli'sche Ungleichung)

\bei Plus-minus-folge: Die Folge $a_n = (-1)^n$ ist nicht konvergent.

\begin{figure*}[h]
\centering
\begin{tikzpicture}
	\draw[thick,<->] (-1.5,0) -- (1.5,0);
	\draw[red,thick] (1,0.1) -- (1,-0.1);
	\draw (0,0.1) -- (0,-0.1);
	\draw[red,thick] (-1,0.1) -- (-1,-0.1);
	\node[below] at (1,0) {$1$};
	\node[below] at (-1,0) {$-1$};
	\node[below] at (0,0) {$0$};
\end{tikzpicture}
\end{figure*}
\lcom{Egal wie groß ich mein $n$ wähle wir bekommen immer $\pm 1$ also divergenz!}























\end{document}